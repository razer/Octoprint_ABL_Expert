# Octoprint ABL Expert
ABL Expert is a plugin for OctoPrint offering auto bed leveling (with a probe) features :
 - Easy changing of the probe offset
 - Preheat before probing
 - Apply probe corrections (see below for further details)

It only work on Marlin firmware with right ABL and probe settings.

For Marlin 1.x it would be :
 - AUTO_BED_LEVELING_BILINEAR
 - EEPROM_SETTINGS
 - X_PROBE_OFFSET_FROM_EXTRUDER
 - Y_PROBE_OFFSET_FROM_EXTRUDER
 - Z_SAFE_HOMING

Starting from Marlin 2.x it would be :
 - AUTO_BED_LEVELING_BILINEAR
 - EEPROM_SETTINGS
 - NOZZLE_TO_PROBE_OFFSET
 - MIN_PROBE_EDGE
 - Z_SAFE_HOMING

The first motivation this plugin came from was to solve probing issues I'm facing on my printer : squeezed X/Y axis, mainly X on my case, resulting on probe values that do not reflects the reality.
During the developping process, I have added some basic features that was usefull for my tests : preheating, probe results, z offset change.

If you're facing probe issues too, better is to read what follow. Otherwise, maybe this plugin can be useful anyway.

# Fixing bed leveling issues
What can be wrong when your first layer is a mess despite a brand new bed probe, that give precise and consistant results ?
Why the left/right part, one or two corners are too high or too low ?

If many auto bed leveling tests results always with the same error, you're probably facing a <b>mechanical issue</b> somewhere, and this plugin was designed for that.
But <b>be aware : for non consistent issues (probe fail...), this tool is useless.</b>

## Step 1: Diagnose your auto bed leveling problem
The first thing to look closely is your <b>probe position</b>. The best way should usually be nearest the nozzle as possible, but sometimes it's better to find a position where one offset is near to 0. That way you should get only one axis causing issues.
In my case I have both axes squeezed, Y the most, and I choose to put my probe 60mm left of my nozzle but aligned with Y axis. Then only X axis have to be fixed.

Then, when you have found the best probe position for you, this method will check is your Z probe offset differs in some parts of X/Y axis. Since it can only be caused by squeeze axis, you will be sure is mechanical :
 - Take note the Z offset value you set in marlin (control -> motion)
 - Be sure to have Bed leveling disabled :
   - `M420S0`
 - Position your nozzle in a zone where leveling problems occurs, then
   manually deploy your Z probe (or activate inductive ones)
   - `M401` or `M280 P0 S10`
 - Move the Z axis down, by 0.1 step at the end, until your probe detect your bed. Take note of the Z height and reset your probe
   - `M280 P0 S160`
   - `M280 P0 S90`
 - Move your nozzle to the position your probe was previously (according with your probe position)
 - Move the Z axis down, with a piece of paper on your bed, until you find the correct height. <b>Do the difference with the previous Z height (probe detection), then compare with your Z offset value from marlin conf (first step)</b>. If the values differs, bad news : your axis is squeeze, good news : this tool was designed for you

## Step 2: Get your printer ready
- Prepare marlin firmware (1.1 bugfix ideally) arduino project
- Enable Bilinear mesh leveling
  - `#define AUTO_BED_LEVELING_BILINEAR`
- Enable Save to EEPROM
  - `#define EEPROM_SETTINGS   // Enable for M500 and M501 commands`
  - `//#define DISABLE_M503    // Comment this line !`
- Be sure to put the right X and Y offset for your probe. <b>Note: values are here as example, you have to define your own</b>
  - `#define X_PROBE_OFFSET_FROM_EXTRUDER 60 // X offset: -left  +right  [of the nozzle]`
  - `#define Y_PROBE_OFFSET_FROM_EXTRUDER 0`
  - On Marlin 2.x : `#define NOZZLE_TO_PROBE_OFFSET { 60, 0, 0 }`
- Fix probe X/Y values to have a mesh centered with your bed. <b>Note: values are here as example, you have to define your own</b>
  - `#define LEFT_PROBE_BED_POSITION 30`
  - `#define RIGHT_PROBE_BED_POSITION 190`
  - `#define FRONT_PROBE_BED_POSITION 30`
  - `#define BACK_PROBE_BED_POSITION 180`
  - On Marlin 2.x, `#define MIN_PROBE_EDGE 15`
- Define left front bed part as Z_SAFE_HOMING
  - `#define Z_SAFE_HOMING`
  - `#define Z_SAFE_HOMING_X_POINT LEFT_PROBE_BED_POSITION    // X point for Z homing when homing all axes (G28).`
  - `#define Z_SAFE_HOMING_Y_POINT FRONT_PROBE_BED_POSITION   // Y point for Z homing when homing all axes (G28).`

## How to set right offset values for axis
- The front left corner of the bed, used as Z_SAFE_HOMING (see Step 2), is the reference point for both axes. Logicaly the offset of this point will be 0, since you will set your Z probe offset from it.
- Correction on both axes can be linear or bilinear
- <b>Linear</b> mean that the offset increase/decrease proportionnaly along the axe. As an example, if <b><i>Right offset</i></b> is set to 1.0 (+1mm), a mesh point in the center of the axe will have 0.5 as offset :
  - <b><i>Right offset</i>: 1.0</b>
  - <b><i>Bilinear correction</i>: unchecked</b>
  - <b>5 points mesh</b> X offsets will be : <b>X0:0, X1:0.25, X2:0.5, X3:0.75, X4:0.1</b>
- <b>Bilinear</b> is for a more complex offset correction. You have to set <b><i>Bilinear correction</i> checked</b>, an index and a value. Let's say you Y axis is squeezed everywhere, but especialy on the very back part. For a 5 mesh point grid, You have to apply a little offset for the 4 first points (let's say .3mm), and have a more consequent one for the last (let's say .6mm). This can be done setting:
  - <b><i>Back offset</i>: 0.6</b>
  - <b><i>Bilinear correction</i>: checked</b>
  - <b><i>Begin at index</i>: 3</b>
  - <b><i>With value</i>: 0.3</b>
  - <b>5 points mesh</b> Y offsets will be: <b>Y0:0, Y1:0.1, Y2:0.2, Y3:0.3, Y4:0.6</b>

You can use the <b>Correction preview</b> to have a result on a default mesh with 0.0 probe values
