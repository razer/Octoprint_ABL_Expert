---
layout: plugin

id: ABL_Expert
title: Auto bed leveling expert
description: Add features for auto bed leveling and z probe handling on Marlin printers
author: razer
license: AGPLv3
date: 2020-02-19

homepage: https://framagit.org/razer/Octoprint_ABL_Expert
source: https://framagit.org/razer/Octoprint_ABL_Expert
archive: https://framagit.org/razer/Octoprint_ABL_Expert/-/archive/0.5/Octoprint_ABL_Expert-0.5.zip

tags:
- probe
- leveling
- bilinear
- auto bed leveling
- abl
- marlin

screenshots:
- url: https://framagit.org/razer/Octoprint_ABL_Expert/blob/master/screenshots/2.png
- url: https://framagit.org/razer/Octoprint_ABL_Expert/blob/master/screenshots/1.png

compatibility:
  octoprint:
  - 1.3.9
  os:
  - linux
  - freebsd
  python: ">=2.7,<4"
---
