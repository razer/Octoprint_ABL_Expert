# Translations template for Auto bed leveling expert.
# Copyright (C) 2018 The OctoPrint Project
# This file is distributed under the same license as the Auto bed leveling
# expert project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Auto bed leveling expert 0.1\n"
"Report-Msgid-Bugs-To: i18n@octoprint.org\n"
"POT-Creation-Date: 2018-08-28 08:02+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:3
msgid "Bed leveling expert"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:7
msgid "Preheating"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:12
msgid "Current actual temperature as reported by your printer"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:12
msgid "Actual"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:13
msgid "Current target temperature as reported by your printer"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:13
msgid "Target"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:16
msgid "Tool"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:21
msgid "Bed"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:29
msgid "Probing"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:34
msgid "Results :"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:40
msgid "Cancel"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:42
msgid "Close"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert.jinja2:47
msgid "Bed leveling"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:3
msgid "General"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:5
msgid "Mesh leveling correction"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:10
msgid "Please connect your printer to get its capabilities"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:12
msgid ""
"Error: Your printer doesn't support some capabilities needed by this "
"plugin, please enable it in"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:13
msgid "your firmware :"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:15
msgid "EEPROM saving"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:16
msgid "Z probe support"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:17
msgid "Bed auto leveling support"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:22
msgid "Preheat"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:24
msgid "Preheat profile values to use"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:27
msgid "Only preheat the bed"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:34
msgid "Cooldown after probing"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:36
msgid "Set temperatures targets to zero after probing"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:47
msgid "Save values in EEPROM (mesh, probe offset)"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:49
msgid "Save data (z offset and bed mesh) in EEPROM"
msgstr ""

#: octoprint_ABL_Expert/templates/ABL_Expert_settings.jinja2:49
msgid ""
"No support EEPROM save, you will not be able to save the bed leveling "
"mesh !"
msgstr ""

